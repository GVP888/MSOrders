package com.ms.orders.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orders")
public class OrdersController {
	
	@Value("${server.port}")
	private String portNo;
	
	@Value("${spring.application.name}")
	private String msName;
	
	@GetMapping("/show")
	public String show() {
		return "Successfully run the"+msName+"-"+portNo;
	}

}
