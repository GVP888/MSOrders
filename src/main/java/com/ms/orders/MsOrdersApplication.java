package com.ms.orders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MsOrdersApplication {
    
	private static final Logger log = LoggerFactory.getLogger(MsOrdersApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(MsOrdersApplication.class, args);
		log.info("Started MS Orders ");
		System.out.println("MsOrdersApplication :: main() ::: Started Prodcut MS - 3102");
		
		System.out.println("Note: Code commited in Gitlab");
	}

}
